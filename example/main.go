package main

import (
	"fmt"
	"os"

	"codeberg.org/Codeberg/avatars"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("please specify a seed string")
		return
	}
	fmt.Println(avatars.MakeAvatar(os.Args[1]))
}
